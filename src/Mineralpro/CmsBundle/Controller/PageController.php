<?php

namespace Mineralpro\CmsBundle\Controller;

use Mineralpro\CmsBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class PageController extends Controller
{
    public function showAction($slug)
    {

    	$page = $this->getDoctrine()->getRepository('MineralproCmsBundle:Page')->findOneBySlug($slug);
    	
        return $this->render('MineralproCmsBundle:Page:show.html.twig', array('page' => $page));
    }
}
