<?php

namespace Mineralpro\CmsBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Mineralpro\CmsBundle\Entity\Brochure;
use Mineralpro\CmsBundle\Form\BrochureType;

/**
 * Brochure controller.
 *
 */
class BrochureController extends Controller
{

    /**
     * Lists all ProductBrochure entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MineralproCmsBundle:Brochure')->findAll();

        return $this->render('MineralproCmsBundle:Admin\Brochure:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new ProductBrochure entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Brochure();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_brochure'));
        }

        return $this->render('MineralproCmsBundle:Admin\Brochure:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a ProductBrochure entity.
     *
     * @param ProductBrochure $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Brochure $entity)
    {
        $form = $this->createForm(new BrochureType(), $entity, array(
            'action' => $this->generateUrl('admin_brochure_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new ProductBrochure entity.
     *
     */
    public function newAction()
    {
        $entity = new Brochure();
        $form   = $this->createCreateForm($entity);

        return $this->render('MineralproCmsBundle:Admin\Brochure:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing Brochure entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MineralproCmsBundle:Brochure')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Brochure entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MineralproCmsBundle:Admin\Brochure:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a ProductBrochure entity.
    *
    * @param ProductBrochure $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Brochure $entity)
    {
        $form = $this->createForm(new BrochureType(), $entity, array(
            'action' => $this->generateUrl('admin_brochure_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing ProductBrochure entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MineralproCmsBundle:Brochure')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Brochure entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_brochure_edit', array('id' => $id)));
        }

        return $this->render('Admin\MineralproCmsBundle:Brochure:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a ProductBrochure entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MineralproCmsBundle:Brochure')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Brochure entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_brochure'));
    }

    /**
     * Creates a form to delete a ProductBrochure entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_brochure_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
