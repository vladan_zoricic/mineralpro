<?php

	namespace Mineralpro\CmsBundle\Controller\Admin;

	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Bundle\FrameworkBundle\Controller\Controller;

	use Mineralpro\CmsBundle\Entity\Home;
	use Mineralpro\CmsBundle\Form\HomeType;

class HomeController extends Controller
{

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MineralproCmsBundle:Home')->findAll();

        return $this->render('MineralproCmsBundle:Admin\Home:index.html.twig', array(
            'entities' => $entities,
        ));
    }


	public function editAction($id)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('MineralproCmsBundle:Home')->find(1);

        $editForm = $this->createEditForm($entity);

		if(!$entity) {
			throw $this->createNotFoundException('Unable to find Home entity');
		}

		return $this->render('MineralproCmsBundle:Admin\Home:edit.html.twig', array('entity' => $entity, 'edit_form' => $editForm->createView()));
	}


	private function createEditForm(Home $entity)
    {
        $form = $this->createForm(new HomeType(), $entity, array(
            'action' => $this->generateUrl('admin_home_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }


    public function updateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MineralproCmsBundle:Home')->find(1);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Home entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);


        if ($editForm->isValid()) {

            $em->flush();

            return $this->redirect($this->generateUrl('admin_home_edit', array('id' => 1)));
        }

    }

}