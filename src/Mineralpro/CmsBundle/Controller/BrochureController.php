<?php

namespace Mineralpro\CmsBundle\Controller;

use Mineralpro\CmsBundle\Entity\Brochure;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BrochureController extends Controller
{
    public function indexAction()
    {
      $brochures = $this->getDoctrine()->getRepository('MineralproCmsBundle:Brochure')->findAll();

        return $this->render('MineralproCmsBundle:Brochure:index.html.twig', array('brochures' => $brochures));
    }
}