<?php

namespace Mineralpro\CmsBundle\Controller;

use Mineralpro\CmsBundle\Entity\Home;
use Mineralpro\CmsBundle\Entity\SliderImage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller
{
    public function showAction()
    {
    	$home = $this->getDoctrine()->getRepository('MineralproCmsBundle:Home')->find(1);
      $companyLogos = $this->getDoctrine()->getRepository('MineralproCmsBundle:Company')->findAll();

        return $this->render('MineralproCmsBundle:Home:show.html.twig', array('home' => $home, 'companyLogos' => $companyLogos));
    }
}