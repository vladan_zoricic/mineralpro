<?php

namespace Mineralpro\CmsBundle\Controller;

use Mineralpro\CmsBundle\Entity\Gallery;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class GalleryController extends Controller
{
    public function showAction()
    {
    	$gallery = $this->getDoctrine()->getRepository('MineralproCmsBundle:Gallery')->findAll();

        return $this->render('MineralproCmsBundle:Gallery:show.html.twig', array('gallery' => $gallery));
    }
}