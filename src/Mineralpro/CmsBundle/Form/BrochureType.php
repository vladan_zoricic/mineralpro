<?php

namespace Mineralpro\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BrochureType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('brochureFile', 'file', array('required' => false))
            ->add('imagePreviewFile', 'file', array('required' => false))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mineralpro\CmsBundle\Entity\Brochure'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mineralpro_cmsbundle_brochure';
    }
}
