<?php

namespace Mineralpro\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;


/**
 * Brochure
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Brochure
{
	/**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="brochurePath", type="string", length=255)
     */
    private $brochurePath;

    private $brochureFile;

    private $brochureTemp;

    /**
     * @var string
     *
     * @ORM\Column(name="imagePreviewPath", type="string", length=255)
     */
    private $imagePreviewPath;

    private $imagePreviewFile;

    private $imagePreviewTemp;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Company
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }



    /**
     * Sets file.
     *
     * @param UploadedFile $brochureFile
     */
    public function setBrochureFile(UploadedFile $brochureFile)
    {
        $this->brochureFile = $brochureFile;
        
        if (isset($this->brochurePath)) {
            $this->brochureTemp = $this->brochurePath;
            $this->brochurePath = null;
        } else {
            $this->brochurePath = 'brochureInitial';
        }
    }

    /**
     * Get file
     *
     * @return UploadedFile
     */
    public function getBrochureFile()
    {
        return $this->brochureFile;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Gallery
     */
    public function setBrochurePath($brochurePath)
    {
        $this->brochurePath = $brochurePath;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getBrochurePath()
    {
        return $this->brochurePath;
    }

    public function getBrochureAbsolutePath()
    {
        return null === $this->brochurePath ? null : $this->getBrochureUploadRootDir().'/'.$this->brochurePath;
    }

    public function getBrochureWebPath()
    {
        return null === $this->brochurePath ? null : $this->getBrochureUploadDir().'/'.$this->brochurePath;
    }

    private function getBrochureUploadRootDir()
    {
        return __DIR__.'/../../../../web/'.$this->getBrochureUploadDir();
    }

    private function getBrochureUploadDir()
    {
        return '/uploads/brochure';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getBrochureFile()) {
            $filename = sha1(uniqid(mt_rand(), true));
            $this->brochurePath = $filename.'.'.$this->getBrochureFile()->guessExtension();
        }
        if (null !== $this->getImagePreviewFile()) {
            $filename = sha1(uniqid(mt_rand(), true));
            $this->imagePreviewPath = $filename.'.'.$this->getImagePreviewFile()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getBrochureFile()) {
            return;
        }

        if (null === $this->getImagePreviewFile()) {
            return;
        }

        $this->getBrochureFile()->move($this->getBrochureUploadRootDir(), $this->brochurePath);

        if (isset($this->brochureTemp)) {
            unlink($this->getBrochureUploadRootDir().'/'.$this->brochureTemp);
            $this->brochureTemp = null;
        }
        $this->brochureFile = null;


        $this->getImagePreviewFile()->move($this->getImagePreviewUploadRootDir(), $this->imagePreviewPath);

        if (isset($this->imagePreviewTemp)) {
            unlink($this->getImagePreviewUploadRootDir().'/'.$this->imagePreviewTemp);
            $this->imagePreviewTemp = null;
        }
        $this->imagePreviewFile = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        $brochureFile = $this->getBrochureAbsolutePath();
        if ($brochureFile) {
            unlink($brochureFile);
        }

        $imagePreviewFile = $this->getImagePreviewAbsolutePath();
        if ($imagePreviewFile) {
            unlink($imagePreviewFile);
        }

    }

    /**
     * Sets file.
     *
     * @param UploadedFile $imagePreviewFile
     */
    public function setImagePreviewFile(UploadedFile $imagePreviewFile)
    {
        $this->imagePreviewFile = $imagePreviewFile;
        
        if (isset($this->imagePreviewPath)) {
            $this->imagePreviewTemp = $this->imagePreviewPath;
            $this->imagePreviewPath = null;
        } else {
            $this->imagePreviewPath = 'imagePreviewInitial';
        }
    }

    /**
     * Get file
     *
     * @return UploadedFile
     */
    public function getImagePreviewFile()
    {
        return $this->imagePreviewFile;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Gallery
     */
    public function setImagePreviewPath($imagePreviewPath)
    {
        $this->imagePreviewPath = $imagePreviewPath;

        return $this;
    }

    // /**
    //  * Get path
    //  *
    //  * @return string 
    //  */
    public function getImagePreviewPath()
    {
        return $this->imagePreviewPath;
    }

    public function getImagePreviewAbsolutePath()
    {
        return null === $this->imagePreviewPath ? null : $this->getImagePreviewUploadRootDir().'/'.$this->imagePreviewPath;
    }

    public function getImagePreviewWebPath()
    {
        return null === $this->imagePreviewPath ? null : $this->getImagePreviewUploadDir().'/'.$this->imagePreviewPath;
    }

    private function getImagePreviewUploadRootDir()
    {
        return __DIR__.'/../../../../web/'.$this->getImagePreviewUploadDir();
    }

    private function getImagePreviewUploadDir()
    {
        return '/uploads/previewImage';
    }
    

}