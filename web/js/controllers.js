var mineralproApp = angular.module('mineralproApp', []).config(function($interpolateProvider){
  $interpolateProvider.startSymbol('{[').endSymbol(']}');
});

mineralproApp.factory('Products', function($http){
  var product = {};

  product.getProducts = function() {
    return $http.get('/articles/list.json');
  }

  return product;

});



mineralproApp.controller('ProductListCtrl', function ($scope, Products) {

  getProducts();

    function getProducts() {
      Products.getProducts()
        .success(function (products) {
            $scope.products = products;
        })
        .error(function (error) {
            $scope.status = 'Unable to load customer data: ' + error.message;
        });
    }
});


mineralproApp.directive('toggleDisabledClass', function () {      
 
    return {
    restrict : 'C',
        link: function(scope, element) {
            element.bind("click" , function(e){
                 element.parent().find("a").removeClass("disabled");
                 element.addClass("disabled");
            });     
        }
    }
});